<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">

<title>KAPints</title>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/heroic-features.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<?php
function getUrlContents($url) {
	$crl = curl_init ();
	
	curl_setopt ( $crl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)' );
	curl_setopt ( $crl, CURLOPT_URL, $url );
	curl_setopt ( $crl, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt ( $crl, CURLOPT_CONNECTTIMEOUT, 5 );
	
	$ret = curl_exec ( $crl );
	curl_close ( $crl );
	return $ret;
}
function getImage($name) {
	if (! file_exists ( 'images/' . utf8_decode ( $name ) . '.jpg' )) {
		
		$json = getUrlContents ( 'http://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=' . urlencode ( $name . ', Karlsruhe' ) );
		$data = json_decode ( $json );
		
		@file_put_contents ( 'images/' . utf8_decode ( $name ) . '.jpg', fopen ( $data->responseData->results [0]->unescapedUrl, 'r' ) );
		if (! file_exists ( 'images/' . utf8_decode ( $name ) . '.jpg' )) {
			return 'images/dummy.jpg';
		}
	}
	
	return 'images/' . rawurlencode ( $name ) . '.jpg';
}
function getFormattedDate($date) {
	return $date->format ( 'd.m.Y' );
}
function readList() {
	$i = 0;
	foreach ( file ( 'termine.txt' ) as $line ) {
		if ((strlen ( trim ( $line ) ) > 0) && (strpos ( $line, '#' ) === false)) {
			
			$parts = explode ( '|', $line );
			
			if (count ( $parts ) == 2) {
				$timestamp = strtotime ( $parts [1] );
				$date = new DateTime ();
				$date->setTimestamp ( $timestamp );
			} else {
				$date = $date->add ( new DateInterval ( 'P7D' ) );
			}
			
			$date1 = new DateTime ();
			$date1->setTimestamp ( $date->getTimestamp () );
			$datas [$i] ['date'] = $date1;
			$datas [$i] ['idx'] = $i + 1;
			
			$parts2 = explode ( ',', $parts [0] );
			if (count ( $parts2 ) == 2) {
				$datas [$i] ['name'] = trim ( $parts2 [0] );
				$datas [$i] ['url'] = trim ( $parts2 [1] );
			} else {
				$datas [$i] ['name'] = trim ( $parts [0] );
			}
			
			$i ++;
		}
	}
	
	return $datas;
}

$datas = readList ();
$index = 0;

?>
<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="./">KAPints</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="?old">All Events</a></li>
				</ul>
				<ul class="nav navbar-nav">
					<li><a href="http://www.kapints.de/cgi-bin/mailman/listinfo/partypeople">Mailingliste</a></li>
				</ul>
				<ul class="nav navbar-nav">
					<li><a href="https://bitbucket.org/dhufnagel/kapints">Source Code</a></li>
				</ul>

			</div>
		</div>
	</nav>

	<!-- Page Content -->
	<div class="container">
	
	<?php
	
	if (isset ( $_GET ['old'] )) {
		include 'old.php';
	} else {
		include 'actual.php';
	}
	?>
	
	<hr>

		<!-- Footer -->
		<footer>
			<div class="row">
				<div class="col-lg-12">
					<p style="text-align: center">Copyright &copy; KAPints 2013-2015</p>
				</div>
			</div>
		</footer>

	</div>
	<!-- /.container -->

	<!-- jQuery -->
	<script src="js/jquery.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.min.js"></script>

</body>

</html>
