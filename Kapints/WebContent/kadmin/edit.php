<?php
$file = '../termine.txt';

// check if form has been submitted
if (isset ( $_POST ['text'] )) {
	// save the text contents
	file_put_contents ( $file, $_POST ['text'] );
}

// read the textfile
$text = file_get_contents ( $file );

?>
<!-- HTML form -->
<form action="" method="post">
	<textarea name="text" style="width: 100%; height: 90%"><?php echo htmlspecialchars($text) ?></textarea>
	<br>
	<br> <input type="submit" value="Speichern"/>
</form>